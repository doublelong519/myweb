from django.shortcuts import render
from django.views import View
from goods.models import *
# Create your views here.
class BaseView(View):
    #模版
    template_name = None
    #数据
    context = {}

    def get(self,request,*args,**kwargs):
        return render(request,self.template_name,self.get_context(request))
    def get_context(self,request):
        self.context.update(self.get_extra_context(request))
        return  self.context
    def get_extra_context(self,request):
        pass

#分页处理，返回多个对象
class MultiobjectReturned(BaseView):
    model = None
    objects_name = 'objects'
    def get_objects(self,page_num='1',per_page = 12 ,objects = None,*args,**kwargs):
        from  django.core.paginator import Paginator
        if objects == None:
            # 创建分页对象，参数为：所有数据，每页数据的个数
            paginator = Paginator(self.model.objects.all(),per_page)
        else:
            paginator = Paginator(objects,per_page)

        page_num = int(page_num)
        if page_num < 1 : page_num =1
        if page_num > paginator.num_pages : page_num = paginator.num_pages

        # 参数：第几页，，，，page.object_list是当前列表页面的值。 page.num是当前也买年的页码
        page = paginator.page(page_num)

        return {'page':page,self.objects_name:page.object_list,'pange_range':paginator.page_range}

class GoodsListView(BaseView,MultiobjectReturned):
    # 所有不变的东西，都放到了类的成员当中
    template_name = 'index.html'
    objects_name = 'goods'
    category_objects = Category.objects.all()

    def prepare(self,request):
        category_id = int(request.GET.get('category',Category.objects.first().id))
        self.objects = Category.objects.get(id=category_id).goods_set.all()
        self.category_id = category_id

    def get_extra_context(self,request):
        page_num = request.GET.get('page',1)
        context = {'category_id':self.category_id,'categorys':self.category_objects}
        context.update(self.get_objects(page_num))
        return context